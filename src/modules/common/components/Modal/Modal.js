import styled from 'styled-components'

export default styled.div`
  position: absolute;
  width: 700px;
  height: 700px;
  top: 50%;
  left: 50%;
  transform: translate(-50%,-50%);
  z-index: 2;
  display: ${props => props.hidden ? 'none' : 'block'}
`
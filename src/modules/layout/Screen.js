import styled from 'styled-components'
const Screen = styled.div`
  flex:1 0 0;
  padding-top:64px;
`
export default Screen

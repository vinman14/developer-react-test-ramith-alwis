import styled from 'styled-components'
import React from 'react'

const StyledModalBody = styled.div`
  width: 100%;
  height: 100%;
  min-height: 500px;
  max-height: 500px;
  overflow-y: scroll;
`

export default ({children}) => {
  return (
    <StyledModalBody className="modal-body">
      {children}
    </StyledModalBody>
  )
}

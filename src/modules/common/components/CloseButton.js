import React from 'react'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
export default (props)=>{
  return(<button {...props} type="button" className="close" aria-label="Close">
    <FontAwesomeIcon aria-hidden="true" className="icon-before" icon={['far', 'times']}/>
  </button>)
}

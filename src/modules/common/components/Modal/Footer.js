import styled from 'styled-components'
import React from 'react'

const StyledFooter = styled.div`
  display: flex;
  flex-direction: column;
  height: fit-content;
  width: 100%;
`

export default ({children}) => {
  return (
    <StyledFooter className="modal-footer">
      {children}
    </StyledFooter>
  )
}
import styled from 'styled-components';
import React from 'react'

const StyledHeader = styled.div`
  background-color: ${props => props.theme.colors.offWhite}
  border-top-left-radius: 15px;
  border-top-right-radius: 15px;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  min-height: 70px;
`

export default ({children}) => {
  return (
    <StyledHeader className='modal-header'>
      {children}
    </StyledHeader>
  )
}
import React from 'react'
import styled from 'styled-components'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'

const CollapsibleInfoStyledComponent = styled.div`
  display: flex;
  justify-content: space-between;
  height: 100%;
  width: 100%;
  align-items: center;
  box-sizing: border-box;
`

const ExpandButton = styled.button`
  border: none;
  background-color: rgba(255,255,255,0);
  outline: none;

  .expand {
    transform: rotate(180deg);
  }
`

const CollapsibleDescription = styled.div`
  width: 100%
  display: flex;
  align-items: center;
`

const CollapsibleInfo = ({children, hidden, toggleVisibility}) => {
  return (
    <CollapsibleInfoStyledComponent>
      <CollapsibleDescription>
        {children}
      </CollapsibleDescription>
      <ExpandButton onClick={toggleVisibility} >
        <FontAwesomeIcon icon={['fal','angle-down']} className={!hidden ? "expand": ''} style={{fontSize: '3rem'}}/>
      </ExpandButton>
    </CollapsibleInfoStyledComponent>
  )
}

export default CollapsibleInfo
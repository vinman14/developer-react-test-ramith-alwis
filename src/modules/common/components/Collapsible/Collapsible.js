import React, {useState} from 'react'
import styled from 'styled-components'
import CollapsibleInfo from './CollapsibleInfo'
import CollapsedContent from './CollapsedContent'

const Collapsible = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  height: fit-content;
  background-color: ${props => props.theme.colors.offWhite}
  border-radius: 5px;
  padding: 12px;
  margin-top: 12px;
`

const CollapsibleComponent = ({visibleContent, collapsedContent}) => {
  const [hidden, setHidden] = useState(true);
  return (
    <Collapsible>
      <CollapsibleInfo  hidden={hidden} toggleVisibility={() => setHidden(!hidden)}>
          {visibleContent}
      </CollapsibleInfo>
      <CollapsedContent hidden={hidden}>
        {collapsedContent}
      </CollapsedContent>
    </Collapsible> 
  )
}

export default CollapsibleComponent
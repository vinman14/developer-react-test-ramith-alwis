import styled from 'styled-components'

export default styled.div`
  padding-top: 12px;
  height: fit-content;
  text-align: left;
  display: ${props => props.hidden ? 'none' : 'flex'}
`

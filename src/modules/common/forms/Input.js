import React from 'react';
import { Control } from 'react-redux-form';

const Input = ({model, label, placeholder, className, required, maxLength}) => {
  if(!className){
    className = ''
  }
  if(!label){
    label = 'Save'
  }
  return (<div className="form-group row">
        <label className="col-sm-2 col-form-label">{label}</label>
        <div className="col-sm-10">
          <Control.text model={model} className="form-control" placeholder={placeholder} required={required} maxLength={maxLength}/>
        </div>
      </div>)
}
export default Input

import styled from 'styled-components'

export default styled.button`
  border-radius: 20px;
  background: ${props => props.disabled ? props.theme.colors.lightGrey : props.theme.colors.white};
  padding: 10px 40px;
  margin-right: 12px;
  color: ${props => props.disabled ? props.theme.colors.white : "#06355F"};
  border: 2px solid ${props => props.disabled ? props.theme.colors.lightGrey : "#06355F"}
  outline: none;
`
import React, {useState} from 'react';
import {Link} from 'react-router-dom'
import {authRequirements} from '../common/constants'
import Title from '../common/components/Title'
import ModalOverlay from '../common/components/Modal/ModalOverlay'
import Modal from '../common/components/Modal/Modal'
import Input from '../common/hookforms/Input'
import Email from '../common/hookforms/Email'
import Submit from '../common/hookforms/Submit'
import Password from '../common/hookforms/Password'
import CenteredScreen from '../layout/CenteredScreen'
import {Form} from 'react-bootstrap'
import useForm from 'react-hook-form'
import styled from 'styled-components'
import AuthBanner from './AuthBanner'
import ModalHeader from '../common/components/Modal/Header'
import ModalBody from '../common/components/Modal/Body'
import ModalFooter from '../common/components/Modal/Footer'
import ModalButton from '../common/components/Modal/ModalButton'
import CollapsibleComponent from '../common/components/Collapsible/Collapsible'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import {Tabs,Tab} from 'react-bootstrap'
const Wrapper = styled(CenteredScreen)`
  .auth-banner{
    width:325px;
    margin-bottom:15px;
    @media (min-width: 768px) {
        width:400px;
    }
  }
  form{
    width:300px;
    text-align:left;
    margin:0 auto 15px;
    .submit{
      margin-top:42px;
      text-align:center;
    }
  }
`

const TabSection = styled.section`
  margin-top: 24px;

  .tab-list > ul > li > a, .tab-list > ul > li > a:hover, .tab-list > ul > li > a:active, .tab-list > ul > li > a:focus {
    border: none;
    outline: none;
    background-color: rgba(255,255,255,0);
  }

  .tab-list > ul > li {
    position: relative;
    border-top: none;
    z-index: 2;
    padding: 0;
    background-color: ${props => props.theme.colors.offWhite}
  }

  .tab-list > ul > li.active {
    border-top: 1px solid ${props => props.theme.colors.black};
    border-left: 1px solid ${props => props.theme.colors.black};
    border-right: 1px solid ${props => props.theme.colors.black};
    border-bottom: none;
    top: 1px;
  }

  .tab-list > ul > li > a {
    padding: 12px;
    color: ${props => props.theme.colors.black};
    width: 100%;
    height: 100%;
  }

  .tab-list > .tab-content {
    border: 1px solid ${props => props.theme.colors.black};
  }

  .tab-list > .nav {
    margin-bottom: 0;
  }
`

const TermsContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;

  input {
    margin-right: 12px;
    border-radius: 0;
  }
`

const ModalActionsContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  margin-top: 12px;
  margin-bottom: 10px;
`

const TabContentContainer = styled.div`
  text-align: left;
`

const StyledLabel = styled.label`
  font-weight: ${props => props.theme.fontWeight}
`

// Placeholder content for Terms and privacy
const placeHolderContent = (
  <React.Fragment>
    <b>What personal information we collect?</b>
    <p>
      Depending on how you engage with Cogniss Services, we collect different kinds of 
      information from or about you.
    </p>
    <b>Account and profile information</b>
    <p>
      We collect information about you when you register for an account, create or modify
    your profile, set preferences, or signup for our Services.
    </p>
    <b>What personal information we collect?</b>
    <p>
      Depending on how you engage with Cogniss Services, we collect different kinds of 
      information from or about you.
    </p>
    <b>Account and profile information</b>
    <p>
      We collect information about you when you register for an account, create or modify
    your profile, set preferences, or signup for our Services.
    </p>
    <b>What personal information we collect?</b>
    <p>
      Depending on how you engage with Cogniss Services, we collect different kinds of 
      information from or about you.
    </p>
    <b>Account and profile information</b>
    <p>
      We collect information about you when you register for an account, create or modify
    your profile, set preferences, or signup for our Services.
    </p>
    <b>What personal information we collect?</b>
    <p>
      Depending on how you engage with Cogniss Services, we collect different kinds of 
      information from or about you.
    </p>
    <b>Account and profile information</b>
    <p>
      We collect information about you when you register for an account, create or modify
    your profile, set preferences, or signup for our Services.
    </p>
  </React.Fragment>
)

const Registration = ({location, history})=> {
  const form = useForm()
  //Handles Modal visibility
  const [modalHidden, setModalHidden] = useState(true);
  //Handles Accept button disabling
  const [disabled, setDisabled] = useState(true);
  const onSubmit = (val) =>{
    setModalHidden(!modalHidden);
  }
  const onlyAlpha = /^\w+$/
  return (<Wrapper className="screen-login">
    <Title title="Log in"/>
    <div className="container-fluid">
      <AuthBanner/>
      <Form horizontal onSubmit={form.handleSubmit(onSubmit)}>
        <Input name="username" form={form} label="Username"
        minLength={authRequirements.USERNAME_MIN} maxLength={authRequirements.USERNAME_MAX}
        pattern={onlyAlpha} patternLabel='Must be only a-z 0-9' required/>
        <Email name="email" form={form} label="Email" required/>
        <Password name="password" form={form} label="Password" required/>
        <p>By clicking Create Account you agree to our <Link to="/terms">Terms</Link> and <Link to="/privacy">Privacy Policy</Link></p>
        <Submit className="submit" label="Create account" form={form} primary/>
      </Form>
      <p>Already have an account? <Link to="login">Sign in now</Link></p>
      <ModalOverlay hidden={modalHidden} onClickHandler={setModalHidden}/>
      <Modal hidden={modalHidden}>
        <div className="modal-content">
          <ModalHeader>
            <h3 style={{marginBottom:'0'}}>Terms and Privacy Policy</h3>
          </ModalHeader>
          <ModalBody>
            <CollapsibleComponent
              visibleContent={
                <React.Fragment>
                  <FontAwesomeIcon icon={['far','info-circle']} style={{fontSize: '2rem'}}/>
                  <h3 style={{marginBottom: "0", marginLeft: "12px"}}>Statement of intent</h3>
                </React.Fragment>
              }
              collapsedContent={
                <React.Fragment>
                  We want you to know exactly how our services work and why we need your details. 
                  Reviewing our policy will help you continue using the app with peace of mind.
                </React.Fragment>
              }
            />
            <TabSection>
              <Tabs defaultActiveKey="terms" className='tabs tab-list' id="termsandprivacy">
                <Tab eventKey="terms" title="Terms of use" tabClassName="nav-tab">
                  <TabContentContainer>
                    {placeHolderContent}
                  </TabContentContainer>
                </Tab>
                <Tab eventKey="privacy" title="Privacy Policy" tabClassName="nav-tab">
                  <TabContentContainer>
                    {placeHolderContent}
                  </TabContentContainer>
                </Tab>
              </Tabs>
            </TabSection>
          </ModalBody>
          <ModalFooter>
            <TermsContainer>
              <input type="checkbox" id="termsconfirmation" onChange={() => setDisabled(!disabled)} style={{transform: 'scale(1.5)'}}/>
              <StyledLabel htmlFor="termsconfirmation">
                I have read and agree to the Terms and Privacy policy
              </StyledLabel>
            </TermsContainer>
            <ModalActionsContainer>
              <ModalButton onClick={() => setModalHidden(!modalHidden)}>Cancel</ModalButton>
              <ModalButton disabled={disabled} onClick={() => setModalHidden(!modalHidden)}>Accept</ModalButton>
            </ModalActionsContainer>
          </ModalFooter>
        </div>
    </Modal>
    </div>
  </Wrapper>)
}
export default Registration

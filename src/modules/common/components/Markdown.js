import React from 'react'
import Markdown from 'react-markdown'
import breaks from 'remark-breaks'
export default (props)=>{
  return <Markdown {...props} plugins={[breaks]} />
}
